module bitbucket.org/sensedia/health-go

go 1.14

require (
	github.com/garyburd/redigo v1.6.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/lib/pq v1.3.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	github.com/stretchr/testify v1.4.0
	go.mongodb.org/mongo-driver v1.3.0
)
