package health

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"runtime"
	"sync"
	"time"
)

type Checker struct {
	mu            sync.Mutex
	checkMap      map[string]Config
	checkerConfig CheckerConfig
}

type CheckerConfig struct {
	//WithSysMetrics is the conditional to process system metrics
	//If it true log and response system metrics
	WithSysMetrics bool
	// ErrorLogFunc is the callback function for errors logging *during check.
	// If not set logging is skipped.
	ErrorLogFunc func(...interface{})
	// DebugLogFunc is the callback function for debug logging during check.
	// If not set logging is skipped.
	DebugLogFunc func(...interface{})
}

func NewChecker(config CheckerConfig) Checker {
	if config.ErrorLogFunc == nil {
		config.ErrorLogFunc = func(...interface{}) {}
	}
	if config.DebugLogFunc == nil {
		config.DebugLogFunc = func(...interface{}) {}
	}

	return Checker{
		mu:            sync.Mutex{},
		checkMap:      make(map[string]Config),
		checkerConfig: config,
	}
}

const (
	statusOK                 = "OK"
	statusPartiallyAvailable = "Partially Available"
	statusUnavailable        = "Unavailable"
	failureTimeout           = "Timeout during health check"
)

type (
	// CheckFunc is the func which executes the check.
	CheckFunc func() error

	// Config carries the parameters to run the check.
	Config struct {
		// Name is the name of the resource to be checked.
		Name string
		// Timeout is the timeout defined for every check.
		Timeout time.Duration
		// SkipOnErr if set to true, it will retrieve StatusOK providing the error message from the failed resource.
		SkipOnErr bool
		// Check is the func which executes the check.
		Check CheckFunc
	}

	// Check represents the health check response.
	Check struct {
		// Status is the check status.
		Status string `json:"status"`
		// Timestamp is the time in which the check occurred.
		Timestamp time.Time `json:"timestamp"`
		// Results holds the checks results
		Results map[string]string `json:"results"`
		// System holds information of the go process.
		*System `json:"system,omitempty"`
	}
	// System runtime variables about the go process.
	System struct {
		// Version is the go version.
		Version string `json:"version"`
		// GoroutinesCount is the number of the current goroutines.
		GoroutinesCount int `json:"goroutines_count"`
		// TotalAllocBytes is the total bytes allocated.
		TotalAllocBytes int `json:"total_alloc_bytes"`
		// HeapObjectsCount is the number of objects in the go heap.
		HeapObjectsCount int `json:"heap_objects_count"`
		// TotalAllocBytes is the bytes allocated and not yet freed.
		AllocBytes int `json:"alloc_bytes"`
	}

	checkResponse struct {
		name      string
		skipOnErr bool
		err       error
	}
)

// Register allot of checks
func (h *Checker) BulkRegister(c ...Config) (err error) {
	for _, cf := range c {
		err = h.Register(cf)
		if err != nil {
			return
		}
	}
	return
}

// Register registers a check config to be performed.
func (h *Checker) Register(c Config) error {
	if c.Timeout == 0 {
		c.Timeout = time.Second * 2
	}

	if c.Name == "" {
		return errors.New("health check must have a name to be registered")
	}

	h.mu.Lock()
	defer h.mu.Unlock()

	if _, ok := h.checkMap[c.Name]; ok {
		return fmt.Errorf("health check %s is already registered", c.Name)
	}

	h.checkMap[c.Name] = c

	return nil
}

// Handler returns an HTTP handler (http.HandlerFunc).
func (h *Checker) Handler() http.Handler {
	return http.HandlerFunc(h.HandlerFunc)
}

// HandlerFunc is the HTTP handler function.
func (h *Checker) HandlerFunc(w http.ResponseWriter, r *http.Request) {
	h.checkerConfig.DebugLogFunc("Handling health check func")
	c := h.ExecuteCheck()
	data, err := json.Marshal(c)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		h.checkerConfig.ErrorLogFunc("Error to parse health check result:\n", err)
		return
	}

	code := http.StatusOK
	if c.Status == statusUnavailable {
		code = http.StatusServiceUnavailable
	}
	w.WriteHeader(code)
	_, _ = w.Write(data)
}

//Execute health check base on config map
func (h *Checker) ExecuteCheck() Check {
	h.mu.Lock()
	defer h.mu.Unlock()

	status := statusOK
	total := len(h.checkMap)
	result := make(map[string]string)
	resChan := make(chan checkResponse, total)

	var wg sync.WaitGroup
	wg.Add(total)

	go func() {
		defer close(resChan)
		wg.Wait()
	}()

	for _, c := range h.checkMap {
		go func(c Config) {
			defer wg.Done()
			select {
			case resChan <- checkResponse{c.Name, c.SkipOnErr, c.Check()}:
			default:
			}
		}(c)

	loop:
		for {
			select {
			case <-time.After(c.Timeout):
				result[c.Name] = statusUnavailable
				h.checkerConfig.ErrorLogFunc("Error on check ", c.Name, ". Error: ", failureTimeout)
				setStatus(&status, c.SkipOnErr)
				break loop
			case res := <-resChan:
				if res.err != nil {
					result[res.name] = statusUnavailable
					h.checkerConfig.ErrorLogFunc("Error on check ", res.name, ". Error: ", res.err.Error())
					setStatus(&status, res.skipOnErr)
				} else {
					result[res.name] = statusOK
				}
				break loop
			}
		}
	}

	c := h.newCheck(status, result)
	return c
}

//Execute health check in standalone which if it is not ok return code 1 to system
func (h *Checker) ExecuteStandalone() {
	c := h.ExecuteCheck()
	h.logCheck(c)
	if c.Status == statusOK {
		os.Exit(0)
	}
	os.Exit(1)
}

// Reset unregisters all previously set check configs
func (h *Checker) Reset() {
	h.mu.Lock()
	h.checkerConfig.DebugLogFunc("Resetting health check configs")
	defer h.mu.Unlock()

	h.checkMap = make(map[string]Config)
}

func (h *Checker) logCheck(c Check) {
	b, e := json.MarshalIndent(c, "", " ")
	if e != nil {
		h.checkerConfig.ErrorLogFunc("Error to parse HealthClient Check Result:\n", e)
	}
	h.checkerConfig.DebugLogFunc("HealthClient Check Result:\n", string(b))
}

func (h *Checker) newCheck(status string, results map[string]string) Check {
	c := Check{
		Status:    status,
		Timestamp: time.Now(),
		Results:   results,
	}

	if h.checkerConfig.WithSysMetrics {
		c.System = newSystemMetrics()
	}

	return c
}

func newSystemMetrics() *System {
	s := runtime.MemStats{}
	runtime.ReadMemStats(&s)

	return &System{
		Version:          runtime.Version(),
		GoroutinesCount:  runtime.NumGoroutine(),
		TotalAllocBytes:  int(s.TotalAlloc),
		HeapObjectsCount: int(s.HeapObjects),
		AllocBytes:       int(s.Alloc),
	}
}

func setStatus(status *string, skipOnErr bool) {
	if skipOnErr && *status != statusUnavailable {
		*status = statusPartiallyAvailable
	} else {
		*status = statusUnavailable
	}
}
